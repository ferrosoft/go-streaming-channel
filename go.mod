module git.ferro.software/ferrosoft/go-streaming-channel

go 1.18

require (
	git.ferro.software/ferrosoft/go-aggregate-error v1.0.1-0.20240618082444-a4e245ba8b55
	github.com/stretchr/testify v1.8.0
)

require (
	github.com/davecgh/go-spew v1.1.1 // indirect
	github.com/pmezard/go-difflib v1.0.0 // indirect
	gopkg.in/yaml.v3 v3.0.1 // indirect
)
