# go-streaming-channel

DRY by encapsulating channel draining logic (producing and consuming items on a
channel). Thanks to Go 1.18 generics it is type safe. See the tests for
examples.

## Origin

This is a fork of https://codeberg.org/sterndata/go-streaming-channel