package streamchan

import (
	"context"

	aggregaterr "git.ferro.software/ferrosoft/go-aggregate-error"
)

type StreamMode int

const (
	// Exhaustible streams are eventually closed during program execution.
	//
	// Canceling the context is considered an error.
	Exhaustible StreamMode = iota+1

	// Infinite streams produce values indefinitely.
	//
	// Canceling the context is not considered an error.
	Infinite
)

type contextKey int

const (
	streamMode contextKey = iota+1
)

// WithStreamMode specifies the stream mode to use for draining.
//
// The default is Exhaustible.
func WithStreamMode(ctx context.Context, mode StreamMode) context.Context {
	return context.WithValue(ctx, streamMode, &mode)
}

// Producer sends items on the stream channel.
//
// The function is intended to be run as go routine. It must take care to close
// the channel, which signals the function is done. Closing is best deferred.
type Producer[T any] func(ctx context.Context, stream chan T) error

// Consumer is invoked on each stream item encountered.
type Consumer[T any] func(ctx context.Context, entity T) error

// Finisher is invoked after all stream items have been processed.
type Finisher func(ctx context.Context) error

// DrainStream invokes handler for every entity T read from repository stream.
//
// Set a stream mode in context to change the handling of canceled context.
func DrainStream[T any](
	ctx context.Context,
	producer Producer[T],
	consumer Consumer[T],
	finisher ...Finisher,
) error {
	var producerErr error
	errs := aggregaterr.New("failed draining stream: ")
	items := make(chan T)

	mode := Exhaustible
	if newMode, ok := ctx.Value(streamMode).(*StreamMode); ok {
		mode = *newMode
	}

	go func() {
		producerErr = producer(ctx, items)
	}()

Drain:
	for {
		select {
		case result, more := <-items:
			if !more {
				break Drain
			}
			err := consumer(ctx, result)
			if err != nil {
				errs.Append(err)
			}
		case <-ctx.Done():
			if mode == Exhaustible {
				errs.Append(ctx.Err())
			}
			break Drain
		}
	}

	if producerErr != nil {
		errs.Append(producerErr)
	}

	for _, finish := range finisher {
		err := finish(ctx)
		if err != nil {
			errs.Append(err)
		}
	}

	return errs.Return()

}
